﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2_ConsoleApp1
{
    /// <summary>
    /// russian:
    /// Класс, описывающий интервал времени в минутах
    /// Предполагается, что 2 разных периода будут равны - если их длительности равны
    /// english:
    /// Class describing time interval in minutes.
    /// It`s supposed that 2 different periods will be equal - if their durations are equal.
    /// </summary>
    public sealed class IntervalMinute
    {
        /// <summary>
        ///  Начало периода
        /// </summary>
        readonly int from;
        /// <summary>
        /// Окончание периода
        /// </summary>
        readonly int to;


        public int From => from;
        public int To => to;

        public IntervalMinute(int from, int to)
        {
            this.from = from;
            this.to = to;
        }

        public int this[int i]
        {
            get
            {
                if (i == 0) return from;
                if (i == 1) return to;
                throw new IndexOutOfRangeException();
            }
        }

        public int GetPeriodInMinutes()
        {
            if (to < from)
                return 0;

            return to - from;
        }

        public static IntervalMinute operator +(IntervalMinute interval, int i)
            => new IntervalMinute(interval.from, interval.to + i);

        public static IntervalMinute operator +(int i, IntervalMinute interval)
            => interval + i;

        public static bool operator ==(IntervalMinute interval1, IntervalMinute interval2)
            //=> interval1.from == interval2.from && interval1.to == interval2.to;
            => interval1.GetPeriodInMinutes() == interval2.GetPeriodInMinutes();

        public static bool operator != (IntervalMinute interval1, IntervalMinute interval2)
            //=> interval1.from != interval2.from || interval1.to != interval2.to;
            => interval1.GetPeriodInMinutes() != interval2.GetPeriodInMinutes();

        public static bool operator > (IntervalMinute interval1, IntervalMinute interval2)
            => interval1.GetPeriodInMinutes() > interval2.GetPeriodInMinutes();

        public static bool operator < (IntervalMinute interval1, IntervalMinute interval2)
            => interval1.GetPeriodInMinutes() < interval2.GetPeriodInMinutes();

        public override string ToString()
        {
            return $"Интервал времени с {from} по {to} минуты";
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;

            IntervalMinute interval = (IntervalMinute)obj;
            return (this.from == interval.from && this.to == interval.to);
        }

        public override int GetHashCode()
        {
            return this.from.GetHashCode() + this.to.GetHashCode();
        }
    }
}