﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2_ConsoleApp1
{
    public static class IntervalExtensionMethod
    {
        public static int SecondsCount(this IntervalMinute interval)
        {
            if (interval.To < interval.From)
                return 0;

            return (interval.To - interval.From) * 60;
        }
    }
}
