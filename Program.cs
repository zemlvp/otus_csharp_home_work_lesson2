﻿using System;

namespace _2_ConsoleApp1
{
    class Program
    {
        private static void Main()
        {
            var interval1 = new IntervalMinute(40, 50);
            
            Console.WriteLine($"interval1 = {interval1}");
            Console.WriteLine($"Длительность interval1 {interval1.GetPeriodInMinutes()} минут (локальный метод)");
            Console.WriteLine($"Длительность interval1 {interval1.SecondsCount()} секунд (т.к. класс IntervalMinute sealed - используем метод расширения IntervalExtensionMethod.SecondsCount)");
            Console.WriteLine($"interval1[0] (from) = {interval1[0]}, interval1[1] (to) = {interval1[1]} (индекс)");
            Console.WriteLine(new string('-', 80));

            Console.WriteLine("interval1 += 10 - Прибавим к интервалу interval1 10 минут");
            interval1 += 10;
            Console.WriteLine($"interval1 = {interval1}");
            var interval2 = interval1;
            Console.WriteLine("interval2 = interval1");
            Console.WriteLine($"interval2 = {interval2}");
            Console.WriteLine($"Длительность interval1 {interval2.GetPeriodInMinutes()} минут (локальный метод)");
            Console.WriteLine($"Длительность interval1 {interval2.SecondsCount()} секунд (метод расширения)");
            Console.WriteLine(new string('-', 80));            

            var interval3 = new IntervalMinute(40, 60);
            Console.WriteLine($"interval2 = {interval2}");
            Console.WriteLine($"interval3 = {interval3}");
            Console.WriteLine($"interval2 == interval3: {(interval2 == interval3).ToString()}");
            Console.WriteLine($"interval2 != interval3: {(interval2 != interval3).ToString()}");
            Console.WriteLine(new string('-', 80));

            var interval4 = new IntervalMinute(100, 140);
            Console.WriteLine($"interval2 = {interval2}");
            Console.WriteLine($"interval4 = {interval4}");
            Console.WriteLine($"interval2 == interval4: {(interval2 == interval4).ToString()}");
            Console.WriteLine($"interval2 != interval4: {(interval2 != interval4).ToString()}");
            Console.WriteLine(new string('-', 80));

            var interval5 = new IntervalMinute(100, 200);
            var interval6 = new IntervalMinute(50, 70);
            Console.WriteLine($"interval5 = {interval5}");
            Console.WriteLine($"interval6 = {interval6}");
            Console.WriteLine($"interval5 > interval6: {(interval5 > interval6).ToString()}");
            Console.WriteLine($"interval5 < interval6: {(interval5 < interval6).ToString()}");

            Console.ReadLine();
        }        
    }
}
